#baseImage
FROM denoland/deno:alpine-1.33.2

#create directory in image
WORKDIR /app

#copy current directiory's files to /app
COPY . /app

#open port in image
EXPOSE 8000

#build image command
RUN deno cache main.ts

#lunach image command
CMD [ "run", "--allow-all", "main.ts" ]
